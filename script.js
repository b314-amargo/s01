/*
1) How do you create arrays in JS?
2) How do you access the first character of an array?
3) How do you access the last character of an array?
4) What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
5) What array method loops over all elements of an array, performing a user-defined function on each iteration?
6) What array method creates a new array with elements obtained from a user-defined function?
7) What array method checks if all its elements satisfy a given condition?
8) What array method checks if at least one of its elements satisfies a given condition?
9) True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
10) True or False: array.slice() copies elements from original array and returns them as a new array.
*/

//==================================================

// #1
let array1 = ["item1", "item2", "item3"];
let array2 = new Array(1, 2, 3);

// #2
// by accessing index[0] of the array
console.log(array1[0]);
console.log(array2[0]);

// #3
// by accessing the array's last index
function lastIndex(arrayName){
	return arrayName.length - 1
}
console.log(array1[lastIndex(array1)]);
console.log(array2[lastIndex(array2)]);

// #4
// indexOf()

// #5
// forEach()

// #6
// map()

// #7
// every()

// #8
// some()

// #9
// false

// #10
// true

//==================================================

/*
Function Coding
	1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
*/
	const students = ["John", "Joe", "Jane", "Jessie"];
	/* 
	Output
	addToEnd(students, "Ryan"); //["John", "Joe", "Jane", "Jessie", "Ryan"]
	addToEnd(students, 045); //"error - can only add strings to an array"
	*/
	function addToEnd(array, item){
		if(typeof item === "string"){
			array.push(item);
			return console.log(array);
		}
		else{
			return console.log("Error - Can only add strings to an array")
		}
	}
	addToEnd(students, "Ryan");
	addToEnd(students, 045);

//======================

/* 
	2) Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.

	//Output
	addToStart(students, "Tess"); //["Tess", "John", "Joe", "Jane", "Jessie", "Ryan"]
	//validation check
	addToStart(students, 033); //"error - can only add strings to an ar
	strings to an array"
*/
	function addToStart(array, item){
		if(typeof item === "string"){
			array.unshift(item);
			return console.log(array);
		}
		else{
			return console.log("Error - Can only add strings to an array")
		}
	}
	addToStart(students, "Tess");
	addToStart(students, 033);

//======================

/*
	3) Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

	//test input
	elementChecker(students, "Jane"); //true
	//validation check
	elementChecker([], "Jane"); //"error - passed in array is empty"
*/
	function elementChecker(array, argument){
		if(array.length == 0){
			return console.log("Error - Passed in array is empty")
		}
		else{
			return console.log(array.includes(argument));
		}
	}
	elementChecker(students, "Jane");
	elementChecker([], "Jane");

//======================

/*
	4) Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:


	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
	if every element in the array ends in the passed in character, return true. Otherwise return false.

	Use the students array and the character "e" as arguments when testing.

	//test input
	checkAllStringsEnding(students, "e"); //false
	//validation checks
	checkAllStringsEnding([], "e"); //"error - array must NOT be empty"
	checkAllStringsEnding(["Jane", 02], "e"); //"error - all array elements must be strings"
	checkAllStringsEnding(students, 4); //"error - 2nd argument must be of data type string"
	checkAllStringsEnding(students, "el"); //"error - 2nd argument must be a single character"
*/
	function checkAllStringsEnding(array, character){
		function check(arr) {
		 	for(var i = 0; i < arr.length; i++){
		   		if(typeof arr[i] != "string") {
		      		return false;
		    	}
		 	}
		 	return true;
		}
		let allEndingWithCharacter = array.every(function(item) {
	        return (item[item.length-1] == character);
	    });

		if(array.length == 0){
			return console.log("Error - Array must NOT be empty")
		}
		else if(check(array) === false){
			return console.log("Error - All array elements must be strings")
		}
		else if(typeof character !== "string"){
			return console.log("Error - 2nd argument must be of data type string")
		}
		else if(character.length !== 1){
			return console.log("Error - 2nd argument must be a single character")
		}
		
		return console.log(allEndingWithCharacter);
	}
	checkAllStringsEnding(students, "e");
	checkAllStringsEnding([], "e");
	checkAllStringsEnding(["Jane", 02], "e");
	checkAllStringsEnding(students, 4);
	checkAllStringsEnding(students, "el");

//======================

/*
	5) Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.

	//test input
	stringLengthSorter(students); //["Joe", "Tess", "John", "Jane", "Ryan", "Jessie"]
	//validation check
	stringLengthSorter([037, "John", 039, "Jane"]); //"error - all array elements must be strings"
*/
	function stringLengthSorter(array){
		function check(arr) {
		 	for(var i = 0; i < arr.length; i++){
		   		if(typeof arr[i] != "string") {
		      		return false;
		    	}
		 	}
		 	return true;
		}

		if(check(array) === false){
			return console.log("Error - All array elements must be strings")
		}
		else{
			return console.log(array.sort((a, b) => a.length - b.length));
		}
	}
	stringLengthSorter(students);
	stringLengthSorter([037, "John", 039, "Jane"]);

//======================

/*
	6) Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:


	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
	return the number of elements in the array that start with the character argument, must be case-insensitive

	Use the students array and the character "J" as arguments when testing.

	//test input
	startsWithCounter(students, "j"); //4
*/
	function startsWithCounter(array, character){
		function check(arr) {
		 	for(var i = 0; i < arr.length; i++){
		   		if(typeof arr[i] != "string") {
		      		return false;
		    	}
		 	}
		 	return true;
		}

		let startWithCharacter = 0

		for(let i = 0; i < array.length; i++){

			let item = array[i];
			let firstCharacter = item[0].toLowerCase();

	   		if(firstCharacter == character.toLowerCase()) {
	      		startWithCharacter++;
	    	}
	 	}

		if(array.length == 0){
			return console.log("Error - Array must NOT be empty")
		}
		else if(check(array) === false){
			return console.log("Error - All array elements must be strings")
		}
		else if(typeof character !== "string"){
			return console.log("Error - 2nd argument must be of data type string")
		}
		else if(character.length !== 1){
			return console.log("Error - 2nd argument must be a single character")
		}
		
		return console.log(startWithCharacter);
	}
	startsWithCounter(students, "j");

//======================

/*
	7) Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:


	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitive

	Use the students array and the string "jo" as arguments when testing.

	//test input
	likeFinder(students, "jo"); //["Joe", "John"]
*/
	function likeFinder(array, argument){
		function check(arr) {
		 	for(var i = 0; i < arr.length; i++){
		   		if(typeof arr[i] != "string") {
		      		return false;
		    	}
		 	}
		 	return true;
		}

		let filteredArray = array.filter(function(item) {
			return item.toLowerCase().includes(argument);
		});

		if(array.length == 0){
			return console.log("Error - Array must NOT be empty")
		}
		else if(check(array) === false){
			return console.log("Error - All array elements must be strings")
		}
		else if(typeof argument !== "string"){
			return console.log("Error - 2nd argument must be of data type string")
		}

		return console.log(filteredArray);
	}
	likeFinder(students, "jo");

//======================

/*
	8) Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.
	//test input
	randomPicker(students); //"Ryan"
	randomPicker(students); //"John"
	randomPicker(students); //"Jessie"
*/
	function randomPicker(array){
		let item = array[Math.floor((Math.random() * array.length))];
		return console.log(item);
	}
	randomPicker(students);
	randomPicker(students);
	randomPicker(students);

//======================

console.log(students);